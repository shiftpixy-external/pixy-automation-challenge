# Installation Instructions for Robot Framework"
1. Install python https://www.python.org/downloads/
2. Check the version with the command: python —version
3. If the version does not appear you may have to set path variables for windows or linux: https://www.tutorialspoint.com/robot_framework/robot_framework_environment_setup.htm. 
4. Check the Pip version to see if you have it with the command: pip —version
5. If you do not have pip, install PIP: https://pip.pypa.io/en/stable/installing/
6. Installing robot framework
    1.  pip install robotframework
7. Check robot version with command: robot —version
8. Use any IDE you like.

# Overview:

1. First you will clone the repository (https://gitlab.com/shiftpixy-external/pixy-automation-challenge).
2. Create a new branch.
3. Once you are satisfied with your solution, push the new branch up to the repo.
4. Submit a merge request.
5. Shift Pixy team will review your results.


# Test Case 1:
Go onto the redfin.com website. Search for any city. Set filters for the search with the following: beds 3-5, property type House, and square feet 5,000. If there are no results, explore with the filters until you see results of 5 or more properties. Assert that the appropriate number of results are displayed.
- Here are the selenium library keywords: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html 
- supportive library:  https://robotframework.org/robotframework/latest/libraries/BuiltIn.html

# Test Case 2:
Go onto the redfin.com website. Hover over your username, select email settings and assert that you are on the email settings page.

# Test Case 3:
Query the API https://jsonplaceholder.typicode.com/posts. Log to console all the the values of the object for the unique “id: 42”
```
{
  "userId": 1,
  "id": 4,
  "title": "eum et est occaecati",
  "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
}
```

Log the following values to the console.
- userId
- id
- title
- body

# Reference
- REST Instance examples: https://pypi.org/project/RESTinstance/
- Here is RESTInstance library keywords: https://asyrjasalo.github.io/RESTinstance/


